import express from 'express'
import Middleware from '../../middlewares'
import PostValidator from '../../validators/PostValidator'
import PostController from '../../controllers/PostController'
import AuthorValidator from '../../validators/AuthorValidator'
import AuthorController from '../../controllers/AuthorController'
import TagController from '../../controllers/TagController'

const router = express.Router()

router.get('/post', PostController.getAllPost)
router.post('/post/:id', PostController.createPost)
router.patch('/post/:id', PostController.updatePost)
router.delete('/post/:id', PostController.deletePost)
router.delete('/post/delete', PostController.deleteAllPost)

router.get('/authors', AuthorController.get)
router.post('/authors', [AuthorValidator.create(), Middleware.Guest], AuthorController.create)
router.patch('/authors/:id', [AuthorValidator.update(), Middleware.Guest], AuthorController.update)
router.delete('/authors/:id', [AuthorValidator.delete(), Middleware.Guest], AuthorController.delete)

router.get('/tag', TagController.getAllTag)
router.post('/tag', TagController.create)
router.patch('/tag/:id', TagController.update)

export default router
