import { Tag } from '../../models'

class TagController {
  static getAllTag = (req, res) => {
    Tag.findAll({ where: {} })
      .then((tags) => {
        res.send(tags)
      })
      .catch((err) => {
        res.status(400).send({ msg: err })
      })
  }

  static create = (req, res) => {
    const { name } = req.body

    return Tag.create({ name })
      .then((tag) => res.send(tag))
      .catch((err) => {
        res.statuc(400).send({ msg: err })
      })
  }

  static update = (req, res) => {
    const { id } = req.params

    return Tag.findOne({
      where: { id },
    }).then(
      (tag) => {
        if (!tag) return res.status(404).json({ message: 'Not found' })

        const { name, gender } = req.body

        return tag.update({ name, gender }, { returning: true })
          .then((updated) => res.status(200).json({ ...updated.dataValues }))
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default TagController
