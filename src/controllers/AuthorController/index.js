import { Author } from '../../models'

class AuthorController {
  static get = (req, res) => Author.findAll().then(
    (data) => res.status(200).json(data),
  ).catch(
    (e) => console.log(e),
  )

  static create = (req, res) => {
    const { name, gender } = req.body

    return Author.create({
      name,
      gender,
    }).then(
      (data) => res.status(201).json({ ...data.dataValues }),
    ).catch(
      (e) => console.log(e),
    )
  }

  static update = (req, res) => {
    const { id } = req.params

    return Author.findOne({
      where: { id },
    }).then(
      (author) => {
        if (!author) return res.status(404).json({ message: 'Not found' })

        const { name, gender } = req.body

        return author.update({ name, gender }, { returning: true })
          .then((updated) => res.status(200).json({ ...updated.dataValues }))
      },
    ).catch(
      (e) => console.log(e),
    )
  }

  static delete = (req, res) => {
    const { id } = req.params

    return Author.destroy({
      where: { id },
      // cascade: true,
      // truncate: false,
    }).then(
      (data) => {
        if (!data) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default AuthorController
