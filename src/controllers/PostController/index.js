// import fs from 'fs'
// import data from '../../data/data.json'
import models from '../../models'

// const filePath = path.resolve(__dirname, '../../data/data.json')
const post = models.Post

class PostController {
  static getAllPost = (req, res) => {
    post.findAll({ where: {} })
      .then((posts) => {
        res.send(posts)
      })
      .catch((err) => {
        res.status(400).send({ msg: err })
      })
  }

  static createPost = (req, res) => {
    post.create({
      id: post.id,
      title: req.body.title,
      body: req.body.body,
      authorId: req.params.id,
    })
      .then((addPost) => res.status(200).send(addPost))
      .catch((err) => res.status(400).send(err))
  }

  static updatePost = (req, res) => {
    post.update({
      title: req.body.title,
      body: req.body.body,
    }, {
      where: {
        id: req.params.id,
      },
    })
      .then(() => res.send({ msg: 'Success' }))
      .catch((err) => res.status(400).send(err))
  }

  static deleteAllPost = (req, res) => {
    post.destroy({
      where: {},
      truncate: true,
      cascade: true,
    })
      .then(() => res.send({ msg: 'post has been deleted' }))
      .catch((err) => ({ msg: err }))
  }

  static deletePost(req, res) {
    const { id } = req.params

    return post.destroy({
      where: { id },
      // cascade: true,
    }).then(
      (data) => {
        if (!data) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default PostController
